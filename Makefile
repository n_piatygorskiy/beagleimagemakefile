
UBOOT_DIR= 	./u-boot
KERNEL_DIR=	./linux
BUSYBOX_DIR=	./busybox
CORES_AMOUNT=	$(shell nproc --all)
KERNEL_VERSION=	linux-5.4.y

all: bootloader kernel busybox install
	echo done
.PHONY: all

start:
	qemu-system-arm -kernel $(BUSYBOX_DIR)/_install/boot/zImage \
	-initrd $(BUSYBOX_DIR)/rootfs.cpio.gz -machine virt -nographic \
	-m 512 -append "root=/dev/ram0 rw console=ttyAMA0,115200 mem=512M"

# building u-boot

LAST_TAG:=		$(shell git -C $(UBOOT_DIR) tag | grep -v rc | tail -1)
CUR_TAG:=		$(shell git -C $(UBOOT_DIR) describe --tags)
export PATH:=		/opt/gcc-linaro-7.5.0-2019.12-x86_64_arm-eabi/bin:$(PATH)
export CROSS_COMPILE=	ccache arm-eabi-
export ARCH=		arm

.PHONY: bootloader
bootloader:	
	git -C $(UBOOT_DIR) checkout $(LAST_TAG)
	make -C $(UBOOT_DIR) am335x_boneblack_vboot_defconfig
	make -C $(UBOOT_DIR) -j$(CORES_AMOUNT)


# building kernel

export PATH:=		/opt/gcc-linaro-7.5.0-2019.12-x86_64_arm-eabi/bin:$(PATH)
export CROSS_COMPILE=	ccache arm-eabi-
export ARCH=		arm
define BBB_CONFIG	
# Enable USB on BBB                                                                                                     CONFIG_USB_ANNOUNCE_NEW_DEVICES=y                                                                                       CONFIG_USB_EHCI_ROOT_HUB_TT=y                                                                                           CONFIG_AM335X_PHY_USB=y                                                                                                 CONFIG_USB_MUSB_TUSB6010=y                                                                                              CONFIG_USB_MUSB_OMAP2PLUS=y                                                                                             CONFIG_USB_MUSB_HDRC=y                                                                                                  CONFIG_USB_MUSB_DSPS=y                                                                                                  CONFIG_USB_MUSB_AM35X=y                                                                                                 CONFIG_USB_CONFIGFS=y                                                                                                   CONFIG_NOP_USB_XCEIV=y                                                                                                  # --- Networking ---                                                                                                    CONFIG_BRIDGE=y                                                                                                         # --- GPIO over sysfs ---                                                                                               CONFIG_GPIO_SYSFS=y                                                                                                     # --- Device Tree Overlays (.dtbo support) ---                                                                          CONFIG_OF_OVERLAY=y                                                                                                     # Hotplug                                                                                                               CONFIG_UEVENT_HELPER=y
endef

.PHONY: kernel
kernel:
	git -C $(KERNEL_DIR) checkout $(KERNEL_VERSION)

	mkdir -p $(KERNEL_DIR)/fragments
	echo "$(BBB_CONFIG)" > $(KERNEL_DIR)/fragments/bbb.cfg

	cd $(KERNEL_DIR); scripts/kconfig/merge_config.sh \
	arch/arm/configs/multi_v7_defconfig \
	fragments/bbb.cfg

	make -C $(KERNEL_DIR) -j$(CORES_AMOUNT) zImage \
	modules am335x-boneblack.dtb am335x-pocketbeagle.dtb


# building busybox
#
BB_LAST_STABLE=		$(shell git -C $(BUSYBOX_DIR) branch -a\
			| grep stable| sort -V| tail -1| sed 's/*//g')	
export PATH:=		/opt/gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf/bin:$(PATH)
export CROSS_COMPILE=	ccache arm-linux-gnueabihf-
export ARCH=		arm

.PHONY: busybox
busybox:
	git -C $(BUSYBOX_DIR) checkout $(BB_LAST_STABLE)
	echo $(PATH)
	make -C $(BUSYBOX_DIR) defconfig
	make -C $(BUSYBOX_DIR) -j$(CORES_AMOUNT)

define INIT_SCRIPT
#!/bin/sh \n\
\n\
mount -t sysfs none /sys \n\
mount -t proc none /proc \n\
mount -t debugfs none /sys/kernel/debug \n\
echo /sbin/mdev > /proc/sys/kernel/hotplug \n\
mdev -s
endef

export INSTALL_MOD_PATH= ./../$(BUSYBOX_DIR)/_install
export ARCH= arm
libc_dir=$$($${CROSS_COMPILE}gcc -print-sysroot)/lib

.PHONY: install
install:
	make -C $(BUSYBOX_DIR) install
	cd $(BUSYBOX_DIR)/_install; mkdir -p boot dev etc/init.d lib proc \
	root sys/kernel/debug 
	echo '$(INIT_SCRIPT)' > $(BUSYBOX_DIR)/_install/etc/init.d/rcS
	chmod +x $(BUSYBOX_DIR)/_install/etc/init.d/rcS
	-cd $(BUSYBOX_DIR); ln -s bin/busybox _install/init

	cd $(BUSYBOX_DIR)/_install/boot; \
	cp ~/repos/linux/arch/arm/boot/zImage .; \
	cp ~/repos/linux/arch/arm/boot/dts/am335x-boneblack.dtb .; \
	cp ~/repos/linux/arch/arm/boot/dts/am335x-pocketbeagle.dtb .; \
	cp ~/repos/linux/System.map .; \
	cp ~/repos/linux/.config ./config
	
	cd $(KERNEL_DIR); make modules_install
	cd $(BUSYBOX_DIR)/_install/lib; \
	cp -a $(libc_dir)/*.so* .

	echo '$$MODALIAS=.* root:toot 660 @modprobe "$$MODALIAS"' > \
	$(BUSYBOX_DIR)/_install/etc/mdev.conf

	cd $(BUSYBOX_DIR)/_install/etc; \
	echo 'root:x:0:' > group; \
	echo 'root:x:0:0:root:/root:/bin/sh' > passwd; \
	echo 'root::10933:0:99999:7:::' > shadow; \
	echo "nameserver 8.8.8.8" > resolv.conf
